import Vue from 'vue';
import Router from 'vue-router';
import Home from './views/Home.vue';
import ChatApp from './views/ChatApp.vue';
import ChatWindow from './views/ChatWindow.vue';
import RoomList from './views/RoomList.vue';
import About from './views/About.vue';

Vue.use(Router);

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      component: ChatApp
    },
    // {
    //   path: '/about',
    //   // name: 'about',
    //   // route level code-splitting
    //   // this generates a separate chunk (about.[hash].js) for this route
    //   // which is lazy-loaded when the route is visited.
    //   component: () => import(/* webpackChunkName: "about" */ './views/About.vue'),
    // },
    // {
    //   path: '/chatApp',
    //   // name: 'chat-app', // ここで指定したクラスに表示される
    //   component: ChatApp,
    // },
  ],
});
